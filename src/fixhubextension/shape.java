package fixhubextension;

public interface shape {
	public int getArea();
	public int getPerimiter();
	public void setWidth(int widthInput);
	public void setHeight(int heightInput);
}

