package fixhubextension;

public class Square implements shape{
	int width;
	int height;
	public Square(int i, int j) {
		// TODO Auto-generated constructor stub
		this.width = i;
		this.height =j;
	}

	@Override
	public int getArea() {
		// TODO Auto-generated method stub
		return this.width*this.height;
	}
	
	@Override
	public int getPerimiter() {
		return 2*this.width + 2*this.height;
	}

	@Override
	public void setWidth(int widthInput) {
		// TODO Auto-generated method stub
		this.width = widthInput;
	}

	@Override
	public void setHeight(int heightInput) {
		// TODO Auto-generated method stub
		this.height = heightInput;
	}
	
}
